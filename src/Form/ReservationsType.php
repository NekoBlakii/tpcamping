<?php

namespace App\Form;

use App\Form\ClientsType;
use App\Entity\Reservations;
use App\Entity\Biens as type;
use App\Entity\Clients as client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ReservationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $datestart = $options['datestart'];
        $dateend = $options['dateend'];
        $ouverture = $options['ouverture'];
        $fermeture = $options['fermeture'];

        $builder
            ->add('date_start', DateType::class,[
                'widget' => 'single_text',
                'attr' => ['id' => 'reservation','min' => $ouverture,'max' =>$fermeture, 'value' => $datestart]
            ])
            ->add('date_end', DateType::class,[
                'widget' => 'single_text',
                'attr' => ['id' => 'reservation','min' =>$ouverture,'max' =>$fermeture, 'value' => $dateend]
            ])
            ->add('nb_adult')
            ->add('nb_child')
            ->add('pool')
            ->add('client', ClientsType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservations::class,
            'datestart' => null,
            'dateend' => null,
            'ouverture' => null,
            'fermeture' => null,
        ]);
    }
}
