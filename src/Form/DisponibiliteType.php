<?php

namespace App\Form;

use App\Entity\Reservations;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class DisponibiliteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_start', DateType::class,[
                'widget' => 'single_text',
                'attr' => ['id' => 'reservation','min' =>'2020-01-01','max' =>'2020-12-31']
            ])
            ->add('date_end', DateType::class,[
                'widget' => 'single_text',
                'attr' => ['id' => 'reservation','min' =>'2020-01-01','max' =>'2020-12-31']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservations::class,
        ]);
    }
}
