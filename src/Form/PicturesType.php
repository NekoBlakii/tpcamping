<?php

namespace App\Form;

use App\Entity\Pictures;
use App\Entity\Templates;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PicturesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('main')
            ->add('template', EntityType::class, [
                'class' => Templates::class,
                'expanded' => false,
                'required' => true,
                'choice_label' => 'title',
                'multiple' => false
            ])
            ->add('imageFile', FileType::class,['required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pictures::class,
        ]);
    }
}
