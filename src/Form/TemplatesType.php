<?php

namespace App\Form;

use App\Entity\Templates;
use App\Entity\Templatetypes as type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TemplatesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price')
            ->add('title')
            ->add('description')
            ->add('max')
            ->add('templatetypes', EntityType::class, [
                'class' => type::class,
                'expanded' => true,
                'required' => true,
                'choice_label' => 'label',
                'multiple' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Templates::class,
        ]);
    }
}
