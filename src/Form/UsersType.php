<?php

namespace App\Form;

use App\Entity\Roles;
use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('password')
            ->add('name')
            ->add('firstname')
            ->add('address')
            ->add('city')
            ->add('tel')
            ->add('role',EntityType::class, [
                'class' => Roles::class,
                'expanded' => true,
                'required' => true,
                'choice_label' => 'label',
                'multiple' => false
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}
