<?php

namespace App\Controller;

use App\Entity\Seasons;
use App\Form\SeasonsType;
use App\Repository\SeasonsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/seasons")
 */
class SeasonsController extends AbstractController
{
    /**
     * @Route("/", name="seasons_index", methods={"GET"})
     */
    public function index(SeasonsRepository $seasonsRepository): Response
    {
        return $this->render('admin/seasons/index.html.twig', [
            'seasons' => $seasonsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="seasons_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $season = new Seasons();
        $form = $this->createForm(SeasonsType::class, $season);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($season);
            $entityManager->flush();

            return $this->redirectToRoute('seasons_index');
        }

        return $this->render('admin/seasons/new.html.twig', [
            'season' => $season,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="seasons_show", methods={"GET"})
     */
    public function show(Seasons $season): Response
    {
        return $this->render('admin/seasons/show.html.twig', [
            'season' => $season,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="seasons_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Seasons $season): Response
    {
        $form = $this->createForm(SeasonsType::class, $season);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('seasons_index');
        }

        return $this->render('admin/seasons/edit.html.twig', [
            'season' => $season,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="seasons_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Seasons $season): Response
    {
        if ($this->isCsrfTokenValid('delete'.$season->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($season);
            $entityManager->flush();
        }

        return $this->redirectToRoute('seasons_index');
    }
}
