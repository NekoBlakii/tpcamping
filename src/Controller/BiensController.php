<?php

namespace App\Controller;

use App\Entity\Biens;
use App\Form\BiensType;
use App\Entity\Templatetypes;
use App\Repository\BiensRepository;
use App\Repository\TemplatesRepository;
use App\Repository\TemplatetypesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/biens")
 */
class BiensController extends AbstractController
{
    private $templatetypesrepo;
    private $templaterepo;

    function __construct(TemplatetypesRepository $templatetypesrepo,TemplatesRepository $templaterepo)
    {
        $this->templatetypesrepo = $templatetypesrepo;
        $this->templaterepo = $templaterepo;
    }

    /**
     * @Route("/", name="biens_index", methods={"GET"})
     */
    public function index(BiensRepository $biensRepository): Response
    {
        return $this->render('admin/biens/index.html.twig', [
            'biens' => $biensRepository->findAll(),
            'templates' => $this->templatetypesrepo->findAll()
        ]);
    }

    /**
     * @Route("/", name="biens_index_post", methods={"POST"})
     */
    public function filter(BiensRepository $biensRepository): Response
    {
        $templates = $this->templaterepo->findBy(array('templatetypes' => $_POST['types']));
        $templateId = [];

        foreach($templates as $template)
        {
            array_push($templateId,$template->getId());
        }

        return $this->render('admin/biens/index.html.twig', [
            'biens' => $biensRepository->findBy(array('template' => $templateId)),
            'templates' => $this->templatetypesrepo->findAll()
        ]);
    }

    /**
     * @Route("admin/new", name="biens_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $bien = new Biens();
        $form = $this->createForm(BiensType::class, $bien);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($bien);
            $entityManager->flush();

            return $this->redirectToRoute('biens_index');
        }

        return $this->render('admin/biens/new.html.twig', [
            'bien' => $bien,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="biens_show", methods={"GET"})
     */
    public function show(Biens $bien): Response
    {
        return $this->render('admin/biens/show.html.twig', [
            'bien' => $bien,
        ]);
    }

    /**
     * @Route("admin/{id}/edit", name="biens_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Biens $bien): Response
    {
        $form = $this->createForm(BiensType::class, $bien);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('biens_index');
        }

        return $this->render('admin/biens/edit.html.twig', [
            'bien' => $bien,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/{id}", name="biens_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Biens $bien): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bien->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($bien);
            $entityManager->flush();
        }

        return $this->redirectToRoute('biens_index');
    }
}
