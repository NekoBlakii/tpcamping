<?php

namespace App\Controller;

use App\Repository\ClientsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

class HomeController extends AbstractController
{
    /**
     *
     * @var \Knp\Snappy\Pdf
     *
     * @return void
     */
    private $knpSnappy;

    function __construct(\Knp\Snappy\Pdf $knpSnappy)
    {
        $this->knpSnappy = $knpSnappy;
    }

    /**
     * @Route("/", name="home")
     *
     */
    public function index()
    {
        return $this->render("home/home.html.twig",[

        ]);
    }

    /**
     * @Route("/about", name="about")
     *
     */
    public function about()
    {
        return $this->render("home/about.html.twig",[

        ]);
    }

    /**
     * @Route("/facture/pdf/{id}", name="facturePDF_client", methods="GET")
     *
     */
    public function facturePDF($id,Request $request, ClientsRepository $clientsRepo)
    {
        $client = $clientsRepo->findOneBy(array('id' => $id));

        $html = $this->renderView('home/facturePDF.html.twig', array(
            'nom' => $client->getName(),
            'prenom' => $client->getFirstName(),
            'id' => $id
        ));

        return new Response(
            $this->knpSnappy->getOutputFromHtml($html), 200,
            [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="facture.pdf"'
            ]
        );
    }

    /**
     * @Route("/facture/{id}", name="facture_client", methods="GET")
     *
     */
    public function facture($id,Request $request, ClientsRepository $clientsRepo)
    {
        $client = $clientsRepo->findOneBy(array('id' => $id));

        return $this->render("home/facture.html.twig",[
            'prenom' => $client->getFirstName(),
            'nom' => $client->getName(),
            'id' => $id
        ]);
    }

    /**
     * @Route("/login", name="login")
     * @param Authentication/Utils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render("home/login.html.twig",[
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

}