<?php

namespace App\Controller;

use App\Entity\Reservations;
use App\Form\ReservationsType;
use App\Repository\BiensRepository;
use App\Repository\ReservationsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/reservation")
 */
class ReservationsController extends AbstractController
{
    /**
     * @Route("/new", name="reservations_new", methods={"GET","POST"})
     */
    public function new(Request $request, BiensRepository $biensRepo): Response
    {
        $resa = new Reservations();
        $form = $this->createForm(ReservationsType::class, $resa,array('datestart' => '2020-01-03', 'dateend' => '2020-01-03', 'ouverture' => '2020-01-01', 'fermeture' => '2020-12-31'));
        $form->handleRequest($request);
        $bien = $biensRepo->findOneBy(array('id' => $_GET['id']));

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $resa->setBien($bien);
            $entityManager->persist($resa);
            $entityManager->flush();

            return $this->redirectToRoute('facture_client',['id' => $resa->getClient()->getId()]);
        }

        return $this->render('home/reservations.html.twig', [
            'resa' => $resa,
            'form' => $form->createView(),
        ]);
    }
}
