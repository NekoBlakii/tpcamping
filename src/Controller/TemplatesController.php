<?php

namespace App\Controller;

use App\Entity\Seasons;
use App\Entity\Templates;
use App\Form\SeasonsType;
use App\Form\TemplatesType;
use App\Entity\Reservations;
use App\Form\ReservationsType;
use App\Form\DisponibiliteType;
use App\Repository\SeaonsRepository;
use App\Repository\TemplatesRepository;
use App\Repository\ReservationsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/templates")
 */
class TemplatesController extends AbstractController
{

    private $reservationsRepo;

    function __construct(ReservationsRepository $reservationsRepo)
    {
        $this->reservationsRepo = $reservationsRepo;
    }

    /**
     * @Route("/", name="templates_index", methods={"GET"})
     */
    public function index(TemplatesRepository $templatesRepository): Response
    {
        return $this->render('admin/templates/index.html.twig', [
            'templates' => $templatesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/new", name="templates_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $template = new Templates();
        $form = $this->createForm(TemplatesType::class, $template);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($template);
            $entityManager->flush();

            return $this->redirectToRoute('templates_index');
        }

        return $this->render('admin/templates/new.html.twig', [
            'template' => $template,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="templates_show", methods={"GET"})
     */
    public function show(Templates $template,Request $request): Response
    {
        $reservation = new Reservations();
        $seasons = new Seasons();
        $form = $this->createForm(DisponibiliteType::class, $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('templates_show',['id' => $template->getId()]);
        }

        return $this->render('admin/templates/show.html.twig', [
            'template' => $template,
            'seasons' => $seasons,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="check_dispo", methods={"POST"})
     */
    public function showDispo(Templates $template,Request $request): Response
    {
        var_dump('DEMANDE ENREGISTREE');
        $seasons = new Seasons();
        $reservation = new Reservations();
        $biens = $template->getBiens();
        //var_dump($biens);
        $arrayReservations = [];
        $form = $this->createForm(DisponibiliteType::class, $reservation);
        $form->handleRequest($request);

        foreach($biens as $value)
        {
            $arrayReservations[] = ['id' => $value->getId(), 'resa' => $value->getReservations()->getValues()];
        }

        $check = null;
        //ICI ON A TOUTES LES RESERVATIONS D'UN BIEN
        foreach($arrayReservations as $key => $resa)
        {
            if($resa['resa'] != null)
            {
                foreach($resa['resa'] as $resaBien) //ON VERIFIE POUR QUE CHAQUE RESERVATION DU BIEN NE SOIT PAS DANS LINTERVALE DU CLIENT
                {
                    //DATE DU CLIENT
                    $dateClientStart = $reservation->getDateStart();
                    $dateClientEnd = $reservation->getDateEnd();


                    //DATE DES BIENS RESERVES
                    $dateBienStart = $resaBien->getDateStart();
                    $dateBienEnd = $resaBien->getDateEnd();

                    //ICI CHECK SI LES DATES SENTRECHOQUENT
                    if($dateClientStart > $dateClientEnd)
                    {
                       var_dump("ERREUR DATE DE DEBUT > DATE DE FIN");
                    }
                    else {
                        if($dateClientStart >= $dateBienStart && $dateClientStart <= $dateBienEnd || $dateClientEnd >= $dateBienStart && $dateClientEnd <= $dateBienEnd)
                        {
                            var_dump('PAS DISPO'); // CE NEST PAS DISPO POUR CE BIEN DONC ON BREAK ET PASSE A LAUTRE BIEN
                            break;
                        }
                        else
                        {
                            return $this->redirectToRoute('reservations_new',['id' => $resaBien->getBien()->getId()]);
                            //CEST BON ON PEUT RESERVER UN BIEN POUR CETTE DATE (récuperer l'ID du bien dispo)
                        }
                    }
                }
            }
            else
            {
                return $this->redirectToRoute('reservations_new',['id' => $resa['id']]);
            }
        }

        return $this->render('admin/templates/show.html.twig', [
            'template' => $template,
            'seasons' => $seasons,
            'form' => $form->createView()
        ]);
    }



    /**
     * @Route("/admin/{id}/edit", name="templates_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Templates $template): Response
    {
        $form = $this->createForm(TemplatesType::class, $template);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('templates_index');
        }

        return $this->render('admin/templates/edit.html.twig', [
            'template' => $template,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}", name="templates_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Templates $template): Response
    {
        if ($this->isCsrfTokenValid('delete'.$template->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($template);
            $entityManager->flush();
        }

        return $this->redirectToRoute('templates_index');
    }
}
