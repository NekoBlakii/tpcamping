<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Biens;
use App\Entity\Roles;
use App\Entity\Users;
use App\Entity\Seasons;
use App\Entity\Pictures;
use App\Entity\Templates;
use App\Entity\Tarifications;
use App\Entity\Templatetypes;
use Doctrine\Migrations\Version\Factory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    /**
     *
     * @var UserPasswordEncoderInterface
     */

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadRole($manager);
        $this->loadUser($manager);
        $this->loadTemplatetype($manager);
        $this->loadTemplate($manager);
        $this->loadBien($manager);
        $this->loadSeason($manager);
        $this->loadPicture($manager);
        $this->loadTarification($manager);

    }

    public function loadRole($manager)
    {
        $roles = ['ROLE_ADMIN','ROLE_OWNER'];
        foreach($roles as $key => $value){
            $role = new Roles();
            $role->setLabel($value);
            $this->setReference('role-' . $key, $role); //Permet de faire un alias d'admin à role-index pour le récuperer plus tard
            $manager->persist($role);
        }
        $manager->flush();
    }

    public function loadUser($manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        $admin = new Users();
        $admin->setRole($this->getReference('role-0'));
        $admin->setEmail('admin@admin.fr');
        $admin->setPassword($this->encoder->encodePassword($admin,'admin'));
        $admin->setName('Philippe');
        $admin->setFirstname('Bernard');
        $admin->setAddress('01 rue des administrateurs');
        $admin->setCity('Perpignan');
        $admin->setTel($faker->phoneNumber);

        $this->setReference('admin',$admin);

        $manager->persist($admin);

        for($i = 0 ; $i < 20 ; $i++)
        {
            $user = new Users();
            $user->setRole($this->getReference('role-1'));
            $user->setEmail($faker->email);
            $user->setPassword($this->encoder->encodePassword($user,'user'));
            $user->setName($faker->lastName);
            $user->setFirstname($faker->firstName);
            $user->setAddress($faker->streetAddress);
            $user->setCity($faker->city);
            $user->setTel($faker->phoneNumber);

            $this->setReference('user-' . $i, $user);

            $manager->persist($user);
        }

        $manager->flush();
    }

    public function loadTemplatetype($manager)
    {
        $types = ['mobilehome','caravane','emplacement'];

        foreach($types as $key => $type)
        {
            $templatetype = new Templatetypes();
            $templatetype->setLabel($type);
            $this->setReference('type-' . $key, $templatetype);
            $manager->persist($templatetype);
        }

        $manager->flush();
    }

    public function loadTemplate($manager)
    {
        $templates = [
                        ['Mobile Home 3 personnes',20,3,'type-0'],
                        ['Mobile Home 4 personnes',24,4,'type-0'],
                        ['Mobile Home 5 personnes',27,5,'type-0'],
                        ['Mobile Home 6-8 personnes',34,8,'type-0'],
                        ['Caravane 2 personnes',15,2,'type-1'],
                        ['Caravane 4 personnes',18,4,'type-1'],
                        ['Caravane 6 personnes',24,6,'type-1'],
                        ['Emplacement 8m²',12,0,'type-2'],
                        ['Emplacement 12m²',14,0,'type-2'],
        ];

        $faker = \Faker\Factory::create('fr_FR');

        foreach($templates as $key => $templateArray)
        {
            $template = new Templates();

            $template->setTitle($templateArray[0]);
            $template->setPrice($templateArray[1]);
            $template->setMax($templateArray[2]);
            $template->setTemplatetypes($this->getReference($templateArray[3]));
            $template->setDescription($faker->text);

            $this->setReference('template-' . $key, $template);

            $manager->persist($template);
        }

        $manager->flush();
    }

    public function loadBien($manager)
    {
        $templateBiens = [
            ['20','admin'],
            ['30','user-'],
            ['10','user-'],
            ['30','user-']
        ];

        foreach($templateBiens as $key => $bienArray)
        {
            for($i = 0 ; $i < $bienArray[0]; $i++)
            {
                $bien = new Biens();
                switch($key)
                {
                    case '0' :
                        $bien->setUser($this->getReference('admin'));
                    case '1' :
                        $bien->setTemplate($this->getReference('template-' . \rand(0, 3)));
                        break;
                    case '2' :
                        $bien->setTemplate($this->getReference('template-' . \rand(4, 6)));
                        break;
                    case '3' :
                        $bien->setTemplate($this->getReference('template-' . \rand(7, 8)));
                        break;
                }
                if($bienArray[1] != "admin")
                {
                    $bien->setUser($this->getReference('user-' . \rand(0, 19)));
                }

                $manager->persist($bien);
            }
        }

        $manager->flush();
    }

    public function loadSeason(ObjectManager $manager)
    {
        $seasonDates = [
            ['ouverture', '2020-05-05', '2020-10-10'],
            ['haute saison', '2020-06-21', '2020-08-31'],
        ];

        foreach ($seasonDates as $key => $value){
            $season = new Seasons();
            $season->setLabel($value[0])
            ->setDateStart(new \DateTime($value[1]))
            ->setDateEnd(new \DateTime($value[2]));

            $manager->persist($season);
        }

        $manager->flush();
    }

    public function loadTarification(ObjectManager $manager)
    {
        $tarification = [
            ['taxe sejour enfant', 0.35, '€'],
            ['taxe sejour adulte', 0.60, '€'],
            ['acces piscine enfant', 1, '€'],
            ['acces piscine adulte', 1.50, '€'],
            ['reduction', 5, '%'],
            ['haute saison', 15, '%'],
        ];

        foreach ($tarification as $key => $value){
            $tarif = new Tarifications();
            $tarif->setCode($value[0])
            ->setValue($value[1])
            ->setType($value[2]);

            $manager->persist($tarif);
        }

        $manager->flush();
    }

    public function loadPicture(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        for($i = 0 ; $i < 9; $i++ ){
            $picture = new Pictures();
            $picture->setTemplate($this->getReference('template-' .$i))
            ->setFilename('pictures/default.png')
            ->setMain(true)
            ->setUpdatedAt(new \DateTime('2020-01-21 00:00:00'));

            $manager->persist($picture);
        }

        $manager->flush();
    }


}
