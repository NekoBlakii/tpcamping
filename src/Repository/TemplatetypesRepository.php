<?php

namespace App\Repository;

use App\Entity\Templatetypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Templatetypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Templatetypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Templatetypes[]    findAll()
 * @method Templatetypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TemplatetypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Templatetypes::class);
    }

    // /**
    //  * @return Templatetypes[] Returns an array of Templatetypes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Templatetypes
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
